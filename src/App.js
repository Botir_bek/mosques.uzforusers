import React from 'react';
import Home from "./pages/Home";
import Header from "./Companents/Header";
import Footer from "./Companents/Footer";
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import PortfolioSections from "./Companents/PortfolioSections";
import ScrollToTop from "./scrollToTop/ScrollToTop";
import SingleMosque from "./Companents/SingleMosque";

function App() {
  return (
    <>
        <Router>
            <Header/>
            {/*<ScrollToTop>*/}
                <Switch>
                    <Route exact path='/' component={Home}/>
                    <Route exact path='/mosques' component={PortfolioSections}/>
                    <Route exact path='/mosque/:id' component={SingleMosque}/>
                </Switch>
            {/*</ScrollToTop>*/}
            <Footer/>
        </Router>

    </>
  );
}

export default App;
