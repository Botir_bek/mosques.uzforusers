import React from 'react';

export default function HeadSection(){
    return(

            <section className="home" id="home">
                <div className="home-content">
                    <div className="container">
                        <span className="hi">Assalomu alaykum va rohmatullohi va barokatuh</span>
                        <h1 className="title">Namozga <br/>
                            <span
                                className="element"
                                data-text1=" masjid qidiryapsizmi?"
                                data-text2=" shoshilyapsizmi"
                                data-loop="true"
                                data-backdelay="3000"
                            >.</span></h1>
                        <p>Siz qayerda bo`lishingizdan qatiiy nazar o`ingizga yaqin masjidni topishingiz va
                            <br/>
                            u qanday ko`rinishda hamda u haqida malumotga ega bo`lishingiz mumkin </p>
                        <div className="social top_30">
                            <a href="#"><i className="fab fa-instagram" aria-hidden="true"></i> </a>
                            <a href="#"><i className="fab fa-telegram" aria-hidden="true"></i> </a>
                            <a href="#"><i className="fab fa-facebook" aria-hidden="true"></i> </a>
                            <a href="#"><i className="fab fa-twitter" aria-hidden="true"></i> </a>
                        </div>
                        {/*Down arrow*/}
                        <a href="#about" className="down-icon wow fadeInUp" data-wow-delay="2.0s">
                            <div className="icon-circle" style={{transform: "matrix(1, 0, 0, 1, 0, 0)"}}></div>
                            <div className="icon" style={{transform: "matrix(1, 0, 0, 1, 0, 0)"}}>
                                <i className="fas fa-angle-down"></i>
                            </div>
                        </a>
                    </div>
                </div>
            </section>

    );
}
