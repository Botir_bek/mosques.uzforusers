import React, { useEffect, useState } from 'react';
// import {useEffect, useState} from "react/cjs/react.production.min";
import axios from "axios";
import ContactUs from "./ContactUs";
import { Link } from "react-router-dom";
import styled from 'styled-components';

const ListItem = styled.li`
    &:hover{
        font-weight:900;
        color:#614ED7;
    }
`
export default function PortfolioSections() {
    const [mosques, setMosques] = useState();


    useEffect(() => {
        getMosques();
        // console.log("Massjidlaer====> ",mosques);

    }, []);
    const regins = [
        { region: "Yunusobod" },
        { region: "Chilonzor" },
        { region: "Uchtepa" },
        { region: "Bektemir" },
        { region: "Olmazor" },
        { region: "Yakkasaroy" },
        { region: "Mirzo Ulug`bek" },
        { region: "Sirg`ali" },
        { region: "Shayxontoxur" },
        { region: "Yashnobot" },
    ]

    async function getMosques() {
        const requset = await axios.get('/api/mosques');
        setMosques(requset.data);

        // console.log(requset);
        // console.log("Massjidlar====> ",mosques.data.mosques);

    }
    async function selectRegion(region) {
        const requset = await axios.get('/api/mosques');
        setMosques(requset.data.filter(item => item.region === region));
        console.log("rr----->", mosques)
    }
    async function allSelect() {
        const requset = await axios.get('/api/mosques');
        setMosques(requset.data);
    }
    return (

        <div id="about">
            <section className="contact" style={{ marginBottom: 100 }} id="contact">
                <div className="container">
                    <div className="section-title bottom_60 text-center">

                        <h2 className="title">Masjidlar kategoriyasi <br /></h2>
                        {/*portfolio filter*/}
                        <div className="portfolio_filter top_30 bottom_60">
                            <ul>
                                <ListItem data-filter="*" onClick={allSelect}>Barchasi</ListItem>
                                {regins.map((item, key) =>
                                    <ListItem key={key} onClick={() => { selectRegion(item.region) }}>{item.region}</ListItem>
                                )}


                            </ul>
                        </div>
                    </div>
                    <div className="row">
                        {/* <Link
                            to="./"
                            className="col-lg-4 col-md-6 blog lifestyle wow fadeInUp"
                            data-wow-delay="0.3s"
                            style={{marginTop:10}}>
                                <img src="assets/img/mosques/kokcha.jpg" alt=""/>
                                <span className="category">KO`RISH</span>
                                <h2 className="title">Ko`kcha jome masjidi</h2>

                        </Link>

                        <Link
                            to="./"
                            className="col-lg-4 col-md-6 blog fashion wow fadeInUp"
                            data-wow-delay="0.6s"
                            style={{marginTop:10}}>
                                <img src="assets/img/mosques/minor_1.jpg" alt=""/>
                                <span className="category">KO`RISH</span>
                                <h2 className="title">Minor jome masjidi</h2>

                        </Link>

                        <Link
                            to="./"
                            className="col-lg-4 col-md-6 blog event wow fadeInUp"
                            data-wow-delay="0.9s"
                            style={{marginTop:10}}>
                                <img src="assets/img/mosques/novza.jpg" alt=""/>
                                <span className="category">KO`RISH</span>
                                <h2 className="title">Islom Ota jome masjidi</h2>
                        </Link> */}
                        {mosques && mosques.map(item => (
                            <Link
                                to={`./mosque/${item.id}`}
                                key={item.id}
                                className="col-lg-4 col-md-6 blog event wow fadeInUp"
                                data-wow-delay="0.9s"
                                style={{ marginTop: 20 }}>
                                <img src={item.imgURL} alt="" height="250" />
                                <span className="category">KO`RISH</span>
                                <h2 className="title">{item.name} jome masjidi</h2>
                                <span className="date">{item.region}</span>
                            </Link>
                        ))}

                    </div>

                </div>
            </section>



        </div>
    );

}
