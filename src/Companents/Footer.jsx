import React from 'react';

export default function Footer(){
    return(
        <div>
            {/*Footer*/}
            <footer className="homef">
                <div className="cont text-center padding_90">
                    <p>Mosques.uz © 2021 Xalqaro islom akademiyasi Informatika va axborot texnalogiyalari <br/> talabasi Jamoliddinov Botirbek/</p>
                    <div className="social top_30">
                        <a href="https://www.facebook.com/"><i className="fab fa-facebook"></i> </a>
                        <a href="https://www.facebook.com/"><i className="fab fa-twitter" aria-hidden="true"></i> </a>
                        <a href="https://www.facebook.com/"><i className="fab fa-instagram" aria-hidden="true"></i> </a>
                        <a href="https://www.facebook.com/"><i className="fab fa-telegram" aria-hidden="true"></i> </a>
                        <a href="https://www.facebook.com/"><i className="fab fa-youtube" aria-hidden="true"></i> </a>
                    </div>
                </div>
            </footer>
        </div>
    );
}
