import React from 'react';
import Loader from '../Companents/Loader'
import {Link} from "react-router-dom";

export default function Header(){
    // const [home]=props;
    return(
        <div>
            <Loader/>
            <header>
                <div className="container">
                    <Link to="/" className="logo">
                        <img style={{width:60}} src="assets/img/Untitled-01.png" alt=""/>
                    </Link>
                    <div className="responsive"><i className="fas fa-bars"></i></div>
                    <nav>
                        <ul>
                            <li><Link to='/'>Asosiy</Link></li>
                            <li><Link to='/mosques'>Masjidlar</Link></li>
                            <li><a href="#services">Xizmatlar</a></li>
                            <li><a href="#blog">Bloglar</a></li>
                            <li><a href="#contact">Bog`lanish</a></li>
                        </ul>
                    </nav>
                </div>
            </header>
            

            <section className="home" id="home">
                <div className="home-content">
                    <div className="container">
                        <span className="hi">Assalomu alaykum va rohmatullohi va barokatuh</span>
                        <h1 className="title">Namozga <br/>
                            <span
                                className="element"
                                data-text1=" masjid qidiryapsizmi?"
                                data-text2=" shoshilyapsizmi"
                                data-loop="true"
                                data-backdelay="3000"
                            >.</span></h1>
                        <p>Siz qayerda bo`lishingizdan qatiiy nazar o`ingizga yaqin masjidni topishingiz va
                            <br/>
                            u qanday ko`rinishda hamda u haqida malumotga ega bo`lishingiz mumkin </p>
                        <div className="social top_30">
                            <a href="https://www.facebook.com/"><i className="fab fa-instagram" aria-hidden="true"></i> </a>
                            <a href="https://www.facebook.com/"><i className="fab fa-telegram" aria-hidden="true"></i> </a>
                            <a href="https://www.facebook.com/"><i className="fab fa-facebook" aria-hidden="true"></i> </a>
                            <a href="https://www.facebook.com/"><i className="fab fa-twitter" aria-hidden="true"></i> </a>
                        </div>
                        {/*Down arrow*/}
                        <a href="#about" className="down-icon wow fadeInUp" data-wow-delay="2.0s">
                            <div className="icon-circle" style={{transform: "matrix(1, 0, 0, 1, 0, 0)"}}></div>
                            <div className="icon" style={{transform: "matrix(1, 0, 0, 1, 0, 0)"}}>
                                <i className="fas fa-angle-down"></i>
                            </div>
                        </a>
                    </div>
                </div>
            </section>
        </div>
    );
}
