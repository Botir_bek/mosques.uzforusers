import React from 'react';
import MainMap from "./MainMap";

export default function AboutSection(){
    return(
      <div>
          <section className="about" id="about">
              <div className="container">
                  <div className="row bottom_90">
                      <div className="col-lg-12 image wow fadeInUp" data-wow-delay="0.4s">
                         
                          <div className="map">
                              <MainMap/>
                          </div>

                      </div>
                      <div className="col-lg-11 offset-lg-1 text wow fadeInUp" data-wow-delay="0.8s">
                          <div className="section-title bottom_15 top_45">
                             
                              <h2 className="title">Toshkent shahridagi masjidlarning xaritada joylashuvi.</h2>
                          </div>
                          <p className="text-center">Quyida Toshkent shahridagi masjidlar joylashuvini YandexMap xaritasi orqali ko`rishingiz mumkin</p>
                          <br/>
                          
                      </div>
                  </div>

                
              </div>
          </section>
      </div>
    );
}
