import React from 'react';

export default function OurServises() {
    return (
        <div>
            <section className="whatwedo padding_90" id="services">
                <div className="container">
                    <div className="section-title bottom_60 text-center">

                        <h2 className="title">Bizning xizmatlar</h2>
                    </div>
                    <div className="row">
                        <div className="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="1.5s">
                            <div className="service">
                                <i className="icon-map"></i>
                                <h3 className="title">Xarita bilan ishlash</h3>
                                <p>Mosques.uz sayti siz uchun o`zida keng qamrovli imkoniyatlarni taklif qiladi.</p>
                            </div>
                        </div>
                        {/*a servise*/}
                        <div className="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="1.8s">
                            <div className="service">
                                <i className="icon-camera"></i>
                                <h3 className="title">Foto albomlar</h3>
                                <p>Mosques.uz sayti siz uchun o`zida keng qamrovli imkoniyatlarni taklif qiladi.</p>
                            </div>
                        </div>
                        {/*a servise*/}
                        <div className="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.6s">
                            <div className="service">
                                <i className="icon-tools"></i>
                                <h3 className="title">Virtual tasvirlar</h3>
                                <p>Mosques.uz sayti siz uchun o`zida keng qamrovli imkoniyatlarni taklif qiladi.</p>
                            </div>
                        </div>
                        {/*a servise*/}
                        <div className="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.3s">
                            <div className="service">
                                <i className="icon-desktop"></i>
                                <h3 className="title">Web loyihalar</h3>
                                <p>Mosques.uz sayti siz uchun o`zida keng qamrovli imkoniyatlarni taklif qiladi.</p>
                            </div>
                        </div>
                        {/*a servise*/}
                        <div className="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.9s">
                            <div className="service">
                                <i className="icon-paintbrush"></i>
                                <h3 className="title">Dizaynrlik salohiyati</h3>
                                <p>Mosques.uz sayti siz uchun o`zida keng qamrovli imkoniyatlarni taklif qiladi.</p>
                            </div>
                        </div>
                        {/*a servise*/}
                        <div className="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="1.2s">
                            <div className="service">
                                <i className="icon-lightbulb"></i>
                                <h3 className="title">Qo`llab quvatlash</h3>
                                <p>Mosques.uz sayti siz uchun o`zida keng qamrovli imkoniyatlarni taklif qiladi.</p>
                            </div>
                        </div>
                        {/*a servise*/}

                    </div>
                </div>
            </section>

        </div>
    );
}
