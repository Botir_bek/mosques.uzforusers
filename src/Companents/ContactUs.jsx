import React from 'react';

export default function ContactUs(){
    return(
        <dev>
            <section className="contact" id="contact">
                <div className="container">
                    <div className="section-title bottom_60 text-center">
                        
                        <h2 className="title">Biz bilan bog`lanish</h2>
                    </div>
                    {/*contact information*/}
                    <div className="information col-lg-8 offset-lg-2">
                        <div className="row">
                            <div className="col-md-4 col-sm-6 info text-center wow fadeInUp" data-wow-delay="0.3s">
                                <i className="icon-phone"></i>
                                <p>+998 99 123 45 67 <br/>+998 99 123 45 67</p>
                            </div>
                            <div className="col-md-4 col-sm-6 info text-center wow fadeInUp" data-wow-delay="0.6s">
                                <i className="icon-map"></i>
                                <p>Shayhontohur A.Qodiriy 11-uy<br/> Toshkent</p>
                            </div>
                            <div className="col-md-4 col-sm-12 info text-center wow fadeInUp" data-wow-delay="0.9s">
                                <i className="icon-envelope"></i>
                                <p>info@gmail.com <br/> contact@bek.com</p>
                            </div>
                        </div>
                    </div>
                    {/*contact form*/}
                    <form className="contact-form top_90 col-md-12 padding_90 wow fadeInUp" data-wow-delay="1.3s">
                        <div className="col-md-8 offset-md-2">
                            <div className="row">
                                <div className="col-md-6">
                                    <input id="con_name" name="con_name" className="form-inp requie" type="text"
                                           placeholder="Name"/>
                                </div>
                                <div className="col-md-6">
                                    <input id="con_email" name="con_email" className="form-inp requie" type="text"
                                           placeholder="Email"/>
                                </div>
                                <div className="col-md-12">
                                    <textarea name="con_message" id="con_message" className="requie"
                                              placeholder="Taklif va murojaatlar?" rows="8"></textarea>
                                    <button id="con_submit" className="site-btn top_30 pull-right" type="submit">Malumotni jo`natish
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </section>

        </dev>
    );
}
