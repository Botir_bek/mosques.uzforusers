import { YMaps, Map, Placemark, FullscreenControl, TypeSelector, ZoomControl } from 'react-yandex-maps';
import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import Marker from '../assets/marker.png'
import axios from "axios";

function MainMap() {
    const [mosques, setMosques] = useState();

    useEffect(() => {
        getMosques();
        // console.log("Massjidlaer====> ",mosques);

    }, []);

    async function getMosques() {
        const requset = await axios.get('/api/mosques');
        setMosques(requset.data);
        // console.log(requset);
        // console.log("Massjidlar====> ",mosques.data.mosques);

    }
    return (
        <YMaps>
            <div>
                {/*My awesome application with maps!*/}

                <Map
                    // style={{
                    //     width:"100%",
                    //     // height:'100%'
                    // }}
                    width={'100%'}
                    height={480}
                    defaultState={{ center: [41.324931, 69.258033], zoom: 11 }}>
                    {/*<Placemark defaultGeometry={[41.324931,69.258033]} />*/}
                    {mosques && mosques.map(item =>
                        <Placemark
                            modules={['geoObject.addon.balloon']}
                            geometry={item.location && item.location.split(',')}
                            properties={{
                                balloonContentBody:
                                    `<Link to=./mosque/${item.id}>
                                        <img width="200" src=${item.imgURL}/>
                                    </Link>
                                    <p>${item.name}</p>`
                            }}
                        />
                    )}
                    <Placemark
                        modules={['geoObject.addon.balloon']}
                        geometry={[41.324931, 69.258033]}
                        properties={{
                            balloonContentBody:
                                `
                                        <img width="200" src="https://masjid.uz/images/stories/masjidlar/yangi-choshtepa/1.jpg"}/>
                                    </Link>
                                    <p>cffdsfsdfsdfdsf</p>`
                        }}
                    />

                    {/*<Placemark*/}
                    {/*    modules={['geoObject.addon.balloon']}*/}
                    {/*    defaultGeometry={[41.2592291,69.1864111]}*/}
                    {/*    options={*/}
                    {/*        `<div>ff</div>`*/}
                    {/*    }*/}
                    {/*    options={{*/}
                    {/*        iconLayout: 'default#image',*/}
                    {/*        iconImageHref:`${Marker}`,*/}
                    {/*        iconImageSize: [40, 40],*/}
                    {/*        iconImageOffset: [-50, -18],*/}
                    {/*    }}*/}
                    {/*    properties={{*/}
                    {/*        hintContent: 'Это хинт',*/}
                    {/*        balloonContentBody:*/}
                    {/*           `<img width="200" src="https://meros.uz/upload/2017/02/7e7cb6814b74d6596098fc80127569a5-large.png"/>*/}
                    {/*              <p>Minor masjidi</p>*/}
                    {/*            `*/}
                    {/*    }}*/}
                    {/*/>*/}
                    <FullscreenControl />
                    <TypeSelector options={{ float: 'left' }} />
                    <ZoomControl options={{ float: 'right' }} />


                </Map>

            </div>
        </YMaps>
    );
}

export default MainMap;
