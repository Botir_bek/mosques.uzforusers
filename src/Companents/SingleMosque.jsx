import React, { useState, useEffect } from 'react';
import axios from "axios";
import { useParams } from 'react-router-dom';
import MainMap from "./MainMap";
import { ReactComponent as Bus } from '../assets/bus.svg';
import { ReactComponent as MiniBus } from '../assets/minibus.svg';
import { ReactComponent as VrSvg } from '../assets/360-degree.svg';
import { ReactComponent as Point } from '../assets/point.svg';
import { ReactComponent as Group } from '../assets/group.svg';
import styled from 'styled-components';
import { FullscreenControl, Map, Placemark, TypeSelector, YMaps, ZoomControl } from "react-yandex-maps";
import Marker from "../assets/marker.png";

const Lists = styled.li`
  div{
    display:flex;
    flex-direction: row;
    h4{
      align-self: center;
    }
  }
  svg{
    width:30px;
    height:30px;
    margin-right:15px;
  }
`;
const ListButton = styled.li`
  margin-top:30px;
  display:flex;
  border: 1px solid #753EA3;
  border-radius:10px;
  background-color: #EFEFEF;
  box-shadow: inset 0px 0px 5px 2px  #753ea375;
  cursor:pointer;
  padding-bottom:5px;
  svg{
    width:50px;
    height:50px;
    margin-left:20px;
  }
  h4{
    
    margin-left:15px;
    align-self: center;
  }
`;
const VRBox = styled.div`
  top:0px;
  width: 100%;
  height: 100vh;
  background-color: rgba(0,0,0,0.8);
  position: fixed;
  z-index: 9999999;
  display: flex;
  justify-content: center;
  iframe{
    align-self: center;
    
  }
  span{
    font-size: 30px;
    color:#fff;
    position: fixed;
    right:50px;
    top:20px;
    
  }
`;
const ImgModal = styled.div`
  top:0px;
  width: 100%;
  height: 100vh;
  background-color: rgba(0,0,0,0.8);
  position: fixed;
  z-index: 9999999;
  display: flex;
  justify-content: center;
  img{
    align-self: center;
    
  }
  span{
    font-size: 30px;
    color:#fff;
    position: fixed;
    right:50px;
    top:20px;
   
  }
`;
const MapBox = styled.div`
    border:8px solid #fefe ;
    border-radius:5px;
    margin-top:10px;
`;

function SingleMosque() {
    const [modal, setModal] = useState(false);
    const [imgView, setimgView] = useState(false);
    const { id } = useParams();
    const [user, setUser] = useState({})

    useEffect(() => {
        loadUser();
        console.log(user);
    }, [])

    const loadUser = async () => {
        const result = await axios.get(`/api/mosques/${id}`);
        setUser(result.data);
        console.log("----response-----")
        console.log(result.data);

    }
    console.log("location---->", user.location && user.location.split(','))

    return (
        <div className="blog-single top_120" id="about">
            <div className="container">
                <div className="row">

                    <div className="col-md-9">
                        <article style={{ marginBottom: 50 }}>
                            <div onClick={() => setModal(img => !img)} className="single-blog-image">
                                <img
                                    className="radius"
                                    src={user.imgURL}
                                    alt={user.name}
                                />
                            </div>
                            <h1 className="single-blog-title top_30 bottom_30">{user.name} jome masjidi</h1>
                            {user.imamName && <b>Masjid imom xatibi: {user.imamName}</b>}
                            {user.info && <p style={{ textAlign: "justify" }}><b>Masjid haqida: </b>{user.info}</p>}
                            <br />
                        </article>


                    </div>

                    <div className="col-md-3">

                        <div className="recent-post">
                            <h3 className="widget-title">Manzil</h3>
                            <ul className="top_15">
                                <Lists>
                                    <a>
                                        <div>
                                            <Point />
                                            <h4 className="title">{user.region}</h4>
                                        </div>
                                        <span>{user.address}</span>
                                    </a>
                                </Lists>
                                <h3 className="widget-title mt-4">Masjid sig`imi</h3>
                                <Lists>
                                    <a>
                                        <div>
                                            <Group />
                                            <h4 className="title">{user.size}</h4>
                                        </div>
                                        <span>Sig`im</span>
                                    </a>
                                </Lists>
                                <h3 className="widget-title mt-4">Mavjud transportlar</h3>
                                <Lists>
                                    <a>
                                        <div>
                                            <Bus />
                                            <h4 className="title">{user.busStop}</h4>
                                        </div>
                                        <span>Avtobuslar</span>
                                    </a>
                                </Lists>
                                <Lists>
                                    <a>
                                        <div>
                                            <MiniBus />
                                            <h4 className="title">{user.miniBus}</h4>
                                        </div>
                                        <span>Mashrutkalar</span>
                                    </a>
                                </Lists>
                                <ListButton onClick={() => setModal(open => !open)}>
                                    <VrSvg />
                                    <h4 className="title">3D VR Tasviri</h4>
                                </ListButton>
                            </ul>
                        </div>


                    </div>
                </div>
                <div style={{ marginBottom: 150 }}>
                    <MapBox>
                        <YMaps>
                            <div>
                                {/*My awesome application with maps!*/}

                                <Map
                                    // style={{
                                    //     width:"100%",
                                    //     // height:'100%'
                                    // }}
                                    width={'100%'}
                                    height={480}
                                    defaultState={{ center: user.location && user.location.split(','), zoom: 11 }}>
                                    {/*<Placemark defaultGeometry={[41.324931,69.258033]} />*/}
                                    <Placemark
                                        modules={['geoObject.addon.balloon']}
                                        defaultGeometry={user.location && user.location.split(',')}
                                        properties={{
                                            balloonContentBody:
                                                `<img width="200" src=${user.imgURL}/>
                                  <p>${user.name}</p>`
                                        }}
                                    />
                                    <FullscreenControl />
                                    <TypeSelector options={{ float: 'left' }} />
                                    <ZoomControl options={{ float: 'right' }} />


                                </Map>

                            </div>
                        </YMaps>
                    </MapBox>
                </div>
            </div>
            {modal &&
                <VRBox>
                    <span onClick={() => setModal(false)}><i className="fas fa-times" ></i></span>
                    <iframe
                        // style={{marginLeft:60}}
                        width="90%"
                        height="80%"
                        frameBorder="0"
                        src={user.vrURL} >
                    </iframe>

                </VRBox>
            }
            {imgView &&
                <ImgModal>
                    <span onClick={() => setimgView(false)}><i className="fas fa-times" ></i></span>
                    <img
                        // style={{marginLeft:60}}
                        width="90%"
                        height="90%"

                        src={user.imgURL} />


                </ImgModal>
            }
        </div>

    );
}



export default SingleMosque;
