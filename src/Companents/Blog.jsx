import React from 'react';
import {Link} from "react-router-dom";

export default function Blog(){
    return(
        <div>
            <section className="blogs gray-bg" id="blog">
                <div className="container">
                    <div className="section-title bottom_60 text-center">
                        <span>Masjidlar</span>
                        <h2 className="title">Toshkent Shahri Masjidlari </h2>
                    </div>
                    <div className="row">
                        {/*Item*/}
                        <Link to="/mosques" className="col-lg-4 col-md-6 blog lifestyle wow fadeInUp"
                           data-wow-delay="0.3s">
                            <img src="assets/img/mosques/kokcha.jpg" alt=""/>
                            <span className="category">LIFESTYLE</span>
                            <h2 className="title">Ko`kcha jome masjidi</h2>
                            <span className="date">Shayhontoxur tumani</span>
                        </Link>
                        {/*Item*/}
                        <Link to="/mosques" className="col-lg-4 col-md-6 blog fashion wow fadeInUp"
                           data-wow-delay="0.6s">
                            <img src="assets/img/mosques/minor_1.jpg" alt=""/>
                            <span className="category">FASHION</span>
                            <h2 className="title">Minor jome masjidi</h2>
                            <span className="date">Yunusobod tumani</span>
                        </Link>
                        {/*Item*/}
                        <Link to="/mosques" className="col-lg-4 col-md-6 blog event wow fadeInUp"
                           data-wow-delay="0.9s">
                            <img src="assets/img/mosques/novza.jpg" alt=""/>
                            <span className="category">EVENT</span>
                            <h2 className="title">Islom ota jome masjid</h2>
                            <span className="date">Yashnobod tumani</span>
                        </Link>
                    </div>
                    <div className="col-md-12 text-center top_90 wow fadeInUp" data-wow-delay="1.2s">
                        <Link to="/mosques" className="site-btn"> Barchasi </Link>
                    </div>
                </div>
            </section>

        </div>
    );
}
