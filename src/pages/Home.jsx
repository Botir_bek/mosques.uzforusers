import React from 'react';
import AboutSection from "../Companents/AboutSection";

import OurServises from "../Companents/OurServises";
import Blog from "../Companents/Blog";
import ContactUs from "../Companents/ContactUs";

export default function Home(){
    return(
      <div>
          {/*About*/}

          <AboutSection/>
          {/*Portfolio*/}
          <Blog/>

          {/*What we do*/}
          <OurServises/>
            {/*Blog*/}
          {/*<PortfolioSections/>*/}

            {/*Contact*/}
          <ContactUs/>
      </div>
    );
}
